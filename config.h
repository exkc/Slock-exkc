
/* text size (must be a valid size) */
static const char * font_name = "6x13";
/* treat a cleared input like a wrong password (color) */
static const int failonclear = 1;

/*Enable blur*/
#define BLUR
/*Set blur radius*/
static const int blurRadius=5;
/*Enable Pixelation*/
//#define PIXELATION
/*Set pixelation radius*/
static const int pixelSize=1;
/* user and group to drop privileges to */
static const char *user  = "human";
static const char *group = "human";

static const char *colorname[NUMCOLS] = {
	[INIT] =   "black",     /* after initialization */
	[INPUT] =  "#bd93f9",   /* during input */
	[TYPEING] =  "#ff79c6",   /* during input */
	[FAILED] = "#ff5555",   /* wrong password */
};

